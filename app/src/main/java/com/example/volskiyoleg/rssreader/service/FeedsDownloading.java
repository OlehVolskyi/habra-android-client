package com.example.volskiyoleg.rssreader.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.example.volskiyoleg.rssreader.R;
import com.example.volskiyoleg.rssreader.activity.MainActivity;
import com.example.volskiyoleg.rssreader.database.DatabaseManager;
import com.example.volskiyoleg.rssreader.event.ServiceStatus;
import com.example.volskiyoleg.rssreader.model.FeedItem;
import com.example.volskiyoleg.rssreader.parser.DataLoader;
import com.example.volskiyoleg.rssreader.parser.XmlParser;

import org.greenrobot.eventbus.EventBus;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;


public class FeedsDownloading extends IntentService {

    public FeedsDownloading() {
        super(FeedsDownloading.class.getName());
    }

    public static int STATUS_FINISH = 1;

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            long oldDatabaseSize = DatabaseManager.getInstance(this).databaseSize();
            Document dataLoader = new DataLoader().getData();
            List<FeedItem> xmlParser = new XmlParser().processXml(dataLoader);
            DatabaseManager.getInstance(this).insertData(xmlParser);
            long newDatabaseSize = DatabaseManager.getInstance(this).databaseSize();

            Log.d("Service", "Service started");
            Log.d("Service", xmlParser.get(xmlParser.size()-1).getLink());
            if (oldDatabaseSize!=newDatabaseSize) {
                Context context = getApplicationContext();
                Intent notificationIntent = new Intent(context, MainActivity.class);
                PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                Notification.Builder builder = new Notification.Builder(this);

                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.drawable.logoimg)
                        .setContentTitle(getString(R.string.notfc_title))
                        .setContentText(getString(R.string.notfc_text));
                Notification notification = builder.getNotification();

                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
                notificationManager.notify(11, notification);
            }
            EventBus.getDefault().post(new ServiceStatus(STATUS_FINISH));
        } catch (IOException | ParserConfigurationException | SAXException | ParseException e1) {
            Log.d("Exception", "Something go wrong in inserting data to database");
        }
    }
}
