package com.example.volskiyoleg.rssreader.activity;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.Toast;

import com.example.volskiyoleg.rssreader.R;
import com.example.volskiyoleg.rssreader.adapter.FeedItemsAdapter;
import com.example.volskiyoleg.rssreader.database.DatabaseManager;
import com.example.volskiyoleg.rssreader.event.ServiceStatus;
import com.example.volskiyoleg.rssreader.loaders.FeedsLoader;
import com.example.volskiyoleg.rssreader.service.FeedsDownloading;
import com.example.volskiyoleg.rssreader.service.Receiver;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, SearchView.OnQueryTextListener {
    private FeedItemsAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Boolean mIsFavorite = false;

    private static final int FEEDS_LOADER = 1;

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (null != searchManager) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return true;
            }
        });

        mAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence constraint) {
                mAdapter.searchText(constraint.toString());
                return DatabaseManager.getInstance(MainActivity.this).findItemByTitle(constraint.toString());
            }
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.news: {
                item.setChecked(true);
                mIsFavorite = false;
                break;
            }
            case R.id.favorites: {
                item.setChecked(true);
                mIsFavorite = true;
                break;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
        updatedAdapter();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ListView listView = (ListView) findViewById(R.id.listView);
        setSupportActionBar(toolbar);
        mAdapter = new FeedItemsAdapter(this, null);
        final Intent intent = new Intent(this, FeedsDownloading.class);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);
        if (listView != null) {
            listView.setAdapter(mAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    NewsDetailPagerActivity.startPagerActivity(MainActivity.this, position, mIsFavorite);
                }
            });
        }
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setColorSchemeColors(R.color.backgroundTitle);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshing(intent);
                Toast.makeText(MainActivity.this, R.string.updated, Toast.LENGTH_SHORT).show();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        getSupportLoaderManager().initLoader(FEEDS_LOADER, null, this);
        refreshing(intent);

        Intent myIntent = new Intent(MainActivity.this, Receiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, myIntent, 0);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime(), 5 * 60 * 1000, pendingIntent);
    }

    private void refreshing(Intent intent) {
        startService(intent);
    }


    @Subscribe
    public void onEvent(ServiceStatus event) {
        if (FeedsDownloading.STATUS_FINISH == event.mServiceStatus) {
            updatedAdapter();
        }
    }


    public void updatedAdapter() {
        getSupportLoaderManager().restartLoader(FEEDS_LOADER, null, this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new FeedsLoader(MainActivity.this, true, mIsFavorite);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mAdapter.getFilter().filter(newText);
        return true;
    }
}