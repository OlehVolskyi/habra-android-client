package com.example.volskiyoleg.rssreader.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.example.volskiyoleg.rssreader.model.FeedItem;

import java.util.List;


public class DatabaseManager {

    private static DatabaseManager sInstance;
    private final Context mContext;

    public static DatabaseManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DatabaseManager(context);
        }
        return sInstance;
    }

    private DatabaseManager(Context context) {
        mContext = context;
    }

    public void insertData(List<FeedItem> feedItems) {
        SQLiteDatabase db = new DatabaseHelper(mContext).getWritableDatabase();
        String sql = "INSERT OR IGNORE INTO items (title,link,pub_date,image) VALUES(?,?,?,?)";
        db.beginTransaction();
        for (int i = 0; i < feedItems.size(); i++) {
            int count = 0;
            SQLiteStatement statement = db.compileStatement(sql);
            statement.bindString(++count, feedItems.get(i).getTitle());
            statement.bindString(++count, feedItems.get(i).getLink());
            statement.bindLong(++count, feedItems.get(i).getPubDate().getTime());
            if (feedItems.get(i).getImageUrl() != null) {
                statement.bindString(++count, feedItems.get(i).getImageUrl());
            } else {
                statement.bindNull(++count);
            }
            statement.execute();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public long databaseSize() {
        SQLiteDatabase db = new DatabaseHelper(mContext).getWritableDatabase();
        SQLiteStatement s = db.compileStatement("select count(*) from " + FeedItem.TABLE);
        return s.simpleQueryForLong();
    }

    public void insertFavoriteData(long itemId, int isOnline) {
        SQLiteDatabase db = new DatabaseHelper(mContext).getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put(FeedItem.COlUMN_FAVORITE, isOnline);
        db.update(FeedItem.TABLE, content, FeedItem.COlUMN_ID + "=?", new String[]{String.valueOf(itemId)});
        db.close();
    }


    public Cursor getFavoritesItems(boolean isFull) {
        SQLiteDatabase db = new DatabaseHelper(mContext).getReadableDatabase();
        if (isFull) {
            return db.rawQuery("SELECT " + FeedItem.COlUMN_ID + ", " + FeedItem.COlUMN_TITLE + ", "
                    + FeedItem.COlUMN_PUB_DATE + ", " + FeedItem.COlUMN_IMAGE + " FROM items WHERE  " +
                    FeedItem.COlUMN_FAVORITE + " = 1 ORDER BY pub_date DESC ", null);
        } else {
            return db.rawQuery("SELECT " + FeedItem.COlUMN_ID + ", " + FeedItem.COlUMN_TITLE + ", " + FeedItem.COlUMN_FAVORITE + ", "
                    + FeedItem.COlUMN_LINK + " FROM items WHERE  " +
                    FeedItem.COlUMN_FAVORITE + " = 1 ORDER BY pub_date DESC ", null);
        }
    }

    public Cursor getFeedItems(boolean isFull) {
        SQLiteDatabase db = new DatabaseHelper(mContext).getReadableDatabase();
        if (isFull) {
            return db.rawQuery("SELECT " + FeedItem.COlUMN_ID + ", " + FeedItem.COlUMN_TITLE + ", "
                    + FeedItem.COlUMN_PUB_DATE + ", " + FeedItem.COlUMN_IMAGE + " FROM items ORDER BY pub_date DESC", null);
        } else {
            return db.rawQuery("SELECT " + FeedItem.COlUMN_ID + ", " + FeedItem.COlUMN_TITLE + ", "
                    + FeedItem.COlUMN_FAVORITE + ", " + FeedItem.COlUMN_LINK + " FROM items ORDER BY pub_date DESC", null);
        }
    }

    public Cursor findItemByTitle(String inputText) throws SQLException {
        SQLiteDatabase db = new DatabaseHelper(mContext).getReadableDatabase();
        Log.w("tag", inputText);
        Cursor mCursor;
        if (inputText == null || inputText.length() == 0) {
            mCursor = db.query(FeedItem.TABLE, new String[]{FeedItem.COlUMN_ID,
                    FeedItem.COlUMN_TITLE, FeedItem.COlUMN_IMAGE, FeedItem.COlUMN_PUB_DATE}, null, null, null, null, "pub_date DESC");

        } else {
            mCursor = db.query(true, FeedItem.TABLE, new String[]{FeedItem.COlUMN_ID,
                            FeedItem.COlUMN_TITLE, FeedItem.COlUMN_IMAGE, FeedItem.COlUMN_PUB_DATE},
                    FeedItem.COlUMN_TITLE + " like '%" + inputText + "%'", null, null, null, "pub_date DESC",null
            );
        }
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    private class DatabaseHelper extends SQLiteOpenHelper {

        private static final String CREATE_ITEMS_TABLE = "CREATE TABLE " + FeedItem.TABLE +
                " ( " + FeedItem.COlUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + FeedItem.COlUMN_FAVORITE + " INTEGER NOT NULL DEFAULT 0, "
                + FeedItem.COlUMN_TITLE + " TEXT, " +
                FeedItem.COlUMN_LINK + " TEXT NOT NULL UNIQUE, "
                + FeedItem.COlUMN_PUB_DATE + " INTEGER, "
                + FeedItem.COlUMN_IMAGE + " TEXT" + ")";

        public DatabaseHelper(Context context) {
            super(context, FeedItem.DATABASE_NAME, null, FeedItem.DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_ITEMS_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}
