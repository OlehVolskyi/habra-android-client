package com.example.volskiyoleg.rssreader.model;

import java.util.Date;

public class FeedItem {

    public static final String DATABASE_NAME = "parserItems.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE = "items";
    public static final String COlUMN_ID = "_id";
    public static final String COlUMN_TITLE = "title";
    public static final String COlUMN_LINK = "link";
    public static final String COlUMN_PUB_DATE = "pub_date";
    public static final String COlUMN_IMAGE = "image";
    public static final String COlUMN_FAVORITE = "favorite";


    private String imageUrl;
    private String mTitle;
    private String mLink;
    private Date mPubDate;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        this.mLink = link;
    }

    public Date getPubDate() {
        return mPubDate;
    }

    public void setPubDate(Date pubDate) {
        this.mPubDate = pubDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
