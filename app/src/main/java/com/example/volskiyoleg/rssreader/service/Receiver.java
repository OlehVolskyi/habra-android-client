package com.example.volskiyoleg.rssreader.service;


import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class Receiver extends BroadcastReceiver {


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onReceive(Context context, Intent intent) {
            Intent service1 = new Intent(context, FeedsDownloading.class);
            context.startService(service1);
    }
}
