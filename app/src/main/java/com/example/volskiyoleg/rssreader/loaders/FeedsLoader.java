package com.example.volskiyoleg.rssreader.loaders;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import com.example.volskiyoleg.rssreader.database.DatabaseManager;


public class FeedsLoader extends CursorLoader {
    private Context mContext;
    private boolean mIsFull;
    private boolean mIsFavorite;

    public FeedsLoader(Context context, Boolean isFull, Boolean isFavorite) {
        super(context);
        mContext = context;
        mIsFull = isFull;
        mIsFavorite = isFavorite;
    }

    @Override
    public Cursor loadInBackground() {
        if (!mIsFavorite) {
            return DatabaseManager.getInstance(mContext).getFeedItems(mIsFull);
        } else {
            return DatabaseManager.getInstance(mContext).getFavoritesItems(mIsFull);
        }
    }
}
