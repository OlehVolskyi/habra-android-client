package com.example.volskiyoleg.rssreader.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.example.volskiyoleg.rssreader.R;
import com.example.volskiyoleg.rssreader.tasks.AddFavoriteItemTask;
import com.example.volskiyoleg.rssreader.web.CustomWebViewClient;

import static com.example.volskiyoleg.rssreader.R.id.action_favourite;


public class NewsDetailsPageFragment extends Fragment {

    private static final String ARGUMENT_ITEM_ID = "item_id";
    private static final String ARGUMENT_LINK = "link";
    private static final String ARGUMENT_FAVORITE = "favorite";


    private long mItemId;
    private String mLink;
    private int mIsFavorite;


    public static NewsDetailsPageFragment newInstance(long itemId, String link, int isFavorite) {
        NewsDetailsPageFragment newsDetailsPageFragment = new NewsDetailsPageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_FAVORITE, isFavorite);
        arguments.putLong(ARGUMENT_ITEM_ID, itemId);
        arguments.putString(ARGUMENT_LINK, link);
        newsDetailsPageFragment.setArguments(arguments);
        return newsDetailsPageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mLink = getArguments().getString(ARGUMENT_LINK);
        mItemId = getArguments().getLong(ARGUMENT_ITEM_ID);
        mIsFavorite = getArguments().getInt(ARGUMENT_FAVORITE);

        android.app.ActionBar actionBar = getActivity().getActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_news_details, menu);
        MenuItem menuItem = menu.findItem(R.id.action_favourite);
        if(mIsFavorite==1){
            menuItem.setIcon(android.R.drawable.btn_star_big_on);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home: {
                super.getActivity().finish();
                return true;
            }
            case action_favourite: {
                int isFavorite;
                if (mIsFavorite == 0) {
                    isFavorite = 1;
                    mIsFavorite = 1;
                    menuItem.setIcon(android.R.drawable.btn_star_big_on);
                } else {
                    menuItem.setIcon(android.R.drawable.btn_star_big_off);
                    mIsFavorite = 0;
                    isFavorite = 0;
                }
                AddFavoriteItemTask addFavoriteItemTask = new AddFavoriteItemTask(getContext(), mItemId, isFavorite);
                addFavoriteItemTask.execute();

                return true;
            }
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.web_view_fragment, null);
        WebView webView = (WebView) view.findViewById(R.id.webViewPage);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new CustomWebViewClient());
        webView.loadUrl(mLink);
        return view;
    }
}
