package com.example.volskiyoleg.rssreader.parser;

import com.example.volskiyoleg.rssreader.model.FeedItem;

import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;

public class XmlParser {

    private static final String TAG_ITEM = "item";
    private static final String TAG_TITLE = "title";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_PUB_DATE = "pubDate";
    private static final String TAG_LINK = "link";
    private static final String TAG_IMAGE = "img";
    private static final String TAG_ATR = "src";
    private static final String DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss Z";

    public List<FeedItem> processXml(Document data) throws IOException, SAXException, ParserConfigurationException, ParseException {
        List<FeedItem> feedItems = new ArrayList<>();

        if (data != null) {
            Element root = data.getDocumentElement();
            Node chanel = root.getChildNodes().item(1);
            NodeList items = chanel.getChildNodes();
            for (int i = 0; i < items.getLength(); i++) {
                Node currentChild = items.item(i);
                if (currentChild.getNodeName().equalsIgnoreCase(TAG_ITEM)) {
                    FeedItem item = new FeedItem();
                    NodeList itemChild = currentChild.getChildNodes();
                    for (int j = 0; j < itemChild.getLength(); j++) {
                        Node current = itemChild.item(j);
                        getItemName(current, item);
                    }
                    feedItems.add(item);
                }
            }
        }
        return feedItems;
    }

    public void getItemName(Node current, FeedItem item) throws ParseException {
        switch (current.getNodeName()) {
            case TAG_TITLE:
                item.setTitle(current.getTextContent());
                break;
            case TAG_DESCRIPTION:
                htmlParser(current, item);
                break;
            case TAG_PUB_DATE:
                Date pubDate = getIntDataFromString(current.getTextContent());
                item.setPubDate(pubDate);
                break;
            case TAG_LINK:
                item.setLink(current.getTextContent());
                break;
        }
    }

    public void htmlParser(Node currentItem, FeedItem item) {
        String htmlData = currentItem.getTextContent();
        org.jsoup.nodes.Document document = Jsoup.parse(htmlData);
        if (htmlData.contains(TAG_IMAGE)) {
            org.jsoup.nodes.Element linkImage = document.select(TAG_IMAGE).first();
            String linkImg = linkImage.attr(TAG_ATR);
            if (linkImg == null) {
                item.setImageUrl(null);
            } else {
                item.setImageUrl(linkImg);
            }
        }
    }

    public Date getIntDataFromString(String pubDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.US);
        return sdf.parse(pubDate);
    }
}