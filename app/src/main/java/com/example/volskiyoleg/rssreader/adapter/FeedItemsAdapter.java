package com.example.volskiyoleg.rssreader.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.widget.CursorAdapter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.volskiyoleg.rssreader.R;
import com.example.volskiyoleg.rssreader.model.FeedItem;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FeedItemsAdapter extends CursorAdapter implements Filterable {
    private static final String DATE_FORMAT = "EEE, dd MMM yyyy HH:mm";
    private LayoutInflater mInflater;
    private String mTextToSearch;


    public FeedItemsAdapter(Context context, Cursor c) {
        super(context, c);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.custom_row_news_item, parent, false);
        ViewHolder holder = new ViewHolder();
        TextView title = (TextView) view.findViewById(R.id.tvTitle);
        TextView date = (TextView) view.findViewById(R.id.tvDate);
        ImageView imageUrl = (ImageView) view.findViewById(R.id.imItemImage);
        holder.mTitle = title;
        holder.mDate = date;
        holder.mImageUrl = imageUrl;
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        String cursorText = cursor.getString(cursor.getColumnIndex(FeedItem.COlUMN_TITLE));
        if (holder != null) {
            holder.mTitle.setText(cursor.getString(cursor.getColumnIndex(FeedItem.COlUMN_TITLE)));
            holder.mDate.setText(longDateToString(new Date(cursor.getLong(cursor.getColumnIndex(FeedItem.COlUMN_PUB_DATE)))));
            if (cursor.getString(cursor.getColumnIndex(FeedItem.COlUMN_IMAGE)) == null) {
                //holder.mImageUrl.setImageResource(R.drawable.logoimg);
            } else {
                Picasso.with(context).load(cursor.getString(cursor.getColumnIndex(FeedItem.COlUMN_IMAGE))).into(holder.mImageUrl);
            }
        }
        SpannableString spannableStringSearch = null;
        if ((mTextToSearch != null) && (!mTextToSearch.isEmpty())) {
            spannableStringSearch = new SpannableString(cursorText);
            Pattern pattern = Pattern.compile(mTextToSearch,
                    Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(cursorText);
            spannableStringSearch.setSpan(new BackgroundColorSpan(
                            Color.TRANSPARENT), 0, spannableStringSearch.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            while (matcher.find()) {
                spannableStringSearch.setSpan(new BackgroundColorSpan(
                                Color.YELLOW), matcher.start(), matcher.end(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        if (spannableStringSearch != null) {
            if (holder != null) {
                holder.mTitle.setText(spannableStringSearch);
            }
        } else {
            if (holder != null) {
                holder.mTitle.setText(cursorText);
            }
        }
    }

    public void searchText(String text) {
        this.mTextToSearch = text;
    }


    public String longDateToString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.US);
        return sdf.format(date);
    }

    public class ViewHolder {
        private TextView mTitle;
        private TextView mDate;
        private ImageView mImageUrl;
    }

}
