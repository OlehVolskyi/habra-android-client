package com.example.volskiyoleg.rssreader.activity;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.example.volskiyoleg.rssreader.R;
import com.example.volskiyoleg.rssreader.adapter.PagerAdapter;
import com.example.volskiyoleg.rssreader.loaders.FeedsLoader;


public class NewsDetailPagerActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int FEEDS_LOADER = 1;
    private static final String CURSOR_POSITION = "position";
    private static final String IS_FAVORITE = "favorite";
    private PagerAdapter mPagerAdapter;
    private Boolean mIsFavorite;

    public static void startPagerActivity(Context context, int position,Boolean isFavorite) {
        Intent intent = new Intent(context, NewsDetailPagerActivity.class);
        intent.putExtra(IS_FAVORITE,isFavorite);
        intent.putExtra(CURSOR_POSITION, position);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        final int cursorPosition = getIntent().getExtras().getInt(CURSOR_POSITION);
        mIsFavorite =getIntent().getExtras().getBoolean(IS_FAVORITE);
        getSupportLoaderManager().initLoader(FEEDS_LOADER, null, this);
        final ViewPager pager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
        if (pager != null) {
            pager.setAdapter(mPagerAdapter);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (pager != null) {
                    pager.setCurrentItem(cursorPosition);
                }
            }
        }, 200);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new FeedsLoader(this, false,mIsFavorite);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mPagerAdapter.swapCursor(data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }


}
