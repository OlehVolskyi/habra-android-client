package com.example.volskiyoleg.rssreader.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.volskiyoleg.rssreader.R;
import com.example.volskiyoleg.rssreader.database.DatabaseManager;

public class AddFavoriteItemTask extends AsyncTask<Void, Void, Void> {

    private final Context mContext;
    private final long mItemId;
    private final int mIsFavorite;

    public AddFavoriteItemTask(Context context, long itemId, int isFavorite) {
        mContext = context;
        mItemId = itemId;
        mIsFavorite = isFavorite;
    }


    @Override
    protected Void doInBackground(Void... params) {
        DatabaseManager.getInstance(mContext).insertFavoriteData(mItemId, mIsFavorite);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (mIsFavorite == 1) {
            Toast.makeText(mContext, R.string.add_favorite, Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(mContext, R.string.remove_favorite, Toast.LENGTH_SHORT).show();
        }
    }
}
