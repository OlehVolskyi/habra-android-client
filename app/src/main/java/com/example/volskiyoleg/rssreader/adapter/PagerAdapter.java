package com.example.volskiyoleg.rssreader.adapter;


import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.volskiyoleg.rssreader.fragments.NewsDetailsPageFragment;
import com.example.volskiyoleg.rssreader.model.FeedItem;


public class PagerAdapter extends FragmentStatePagerAdapter {
    public Cursor mCursor;
    private int pageCount;


    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void swapCursor(Cursor cursor) {
        mCursor = cursor;
        pageCount = cursor.getCount();
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        mCursor.moveToPosition(position);
        String link = mCursor.getString(mCursor.getColumnIndex(FeedItem.COlUMN_LINK));
        long id = mCursor.getLong(mCursor.getColumnIndex(FeedItem.COlUMN_ID));
        int isFavorite = mCursor.getInt(mCursor.getColumnIndex(FeedItem.COlUMN_FAVORITE));
        return NewsDetailsPageFragment.newInstance(id,link,isFavorite);
    }

    @Override
    public int getCount() {
        return pageCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        mCursor.moveToPosition(position);
        return  mCursor.getString(mCursor.getColumnIndex(FeedItem.COlUMN_TITLE));
    }
}